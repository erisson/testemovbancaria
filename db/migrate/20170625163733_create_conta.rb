class CreateConta < ActiveRecord::Migration[5.0]
  def change
    create_table :conta do |t|
      t.string :banco
      t.string :conta_corrent
      t.string :agencia
      t.decimal :saldo

      t.timestamps
    end
  end
end
