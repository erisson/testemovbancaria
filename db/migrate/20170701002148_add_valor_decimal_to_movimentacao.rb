class AddValorDecimalToMovimentacao < ActiveRecord::Migration[5.0]
  def change
  	add_column :movimentacaos, :valor, :decimal, :default => 0
  end
end
