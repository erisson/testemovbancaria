class AddAtivoBooleanToConta < ActiveRecord::Migration[5.0]
  def change
  	add_column :conta, :ativo, :boolean, :default => true

  end
end
