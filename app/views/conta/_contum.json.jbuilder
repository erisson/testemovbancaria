json.extract! contum, :id, :banco, :conta_corrent, :agencia, :saldo, :created_at, :updated_at
json.url contum_url(contum, format: :json)
