class ApplicationController < ActionController::Base
  protect_from_forgery with: :exception

  #para exigir autenticacao em qualquer tela
  before_action :authenticate_user!
end
