require 'rails_helper'

RSpec.describe Contum, type: :model do
  #pending "add some examples to (or delete) #{__FILE__}"

  it "eh_possivel_salvar_uma_conta" do 
  	conta = Contum.create!(banco: 'Santander', conta_corrent: '112233-0', agencia: '4433', saldo: 100) 
  	expect(conta).to eq(Contum.find(conta.id))
  end

  it "nao_eh_possivel_salvar_uma_conta_sem_informar_o_banco" do 
  	conta = Contum.new(conta_corrent: '112233-0', agencia: '4433', saldo: 100)
  	conta.save 
	expect(conta.errors[:banco]).to include("não pode ficar em branco")
  end

  it "nao_eh_possivel_salvar_uma_conta_sem_informar_a_conta_corrente" do 
  	conta = Contum.new(banco: 'Santander', agencia: '4433', saldo: 100)
  	conta.save 
	expect(conta.errors[:conta_corrent]).to include("não pode ficar em branco")
  end

  it "nao_eh_possivel_salvar_uma_conta_sem_informar_a_agencia" do 
  	conta = Contum.new(banco: 'Santander', conta_corrent: '112233-0', saldo: 100)
  	conta.save 
	expect(conta.errors[:agencia]).to include("não pode ficar em branco")
  end

  it "nao_eh_possivel_salvar_uma_conta_sem_informar_o_saldo" do 
  	conta = Contum.new(banco: 'Santander', conta_corrent: '112233-0', agencia: '4433')
  	conta.save 
	expect(conta.errors[:saldo]).to include("não pode ficar em branco")
  end

end
