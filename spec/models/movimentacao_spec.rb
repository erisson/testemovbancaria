require 'rails_helper'

RSpec.describe Movimentacao, type: :model do
  #pending "add some examples to (or delete) #{__FILE__}"

  ###Testes abaixo são para o trata_movimentacao

  it "eh_invalido_sem_a_conta_origem" do 
  	mov = Movimentacao.new(conta: nil) 
  	mov.trata_movimentacao  
  	expect(mov.errors[:conta]).to include("Informe a conta de origem!") 
  end

  it "valor_nao_pode_ser_negativo" do
  	@conta = Contum.new(id: 1) 
  	mov = Movimentacao.new(conta: @conta, valor: -1) 
  	mov.trata_movimentacao  
  	expect(mov.errors[:valor]).to include("não pode ser negativo!") 
  end

  it "se_transferencia_Informe_a_conta_receptora" do
  	@conta = Contum.new(id: 1) 
  	mov = Movimentacao.new(conta: @conta, valor: 1, tranferencia: true) 
  	mov.trata_movimentacao  
  	expect(mov.errors[:conta_receptora_id]).to include("Informe a conta para transferência!") 
  end

  it "se_saque_o_saldo_nao_pode_ficar_negativo" do
   	@conta = Contum.create(id: 2, banco: 'Santander', conta_corrent: '112233-0', agencia: '4433', saldo: 10)  
   	mov = Movimentacao.new(conta: @conta, valor: 1, saque: true, valor: 15) 
   	mov.trata_movimentacao  
   	expect(mov.errors[:valor]).to include("Saldo não pode ficar negativo!") 
  end

end
